# Flat Finder

This repository handles the CD pipelines for the [Flat Finder](https://m-c-moore.gitlab.io/flat-finder) website. The source code is hosted privately [here](https://gitlab.com/m-c-moore/flat-finder).

The pipelines are run every hour, although searches are only performed between 8am and 8pm in the local timezone. For more information, please see the [about page](https://m-c-moore.gitlab.io/flat-finder/#/about) or [get in touch](mailto:matt.moore.dev@gmail.com).
